(function() {
    'use strict';

    angular
        .module('app')
        .controller('Ctrl', Ctrl);

    Ctrl.$inject = [];

    function Ctrl() {

        var ctrl = {
            func: func
        };

        activate();

        angular.extend(this, ctrl);

        //////////////////////

        function activate() {
        }

        /*------------------*/

        function func() {
        }
    }
})();