(function() {
    'use strict';

    angular
        .module('app')
        .directive('directive', directive);

    directive.$inject = [];

    function directive() {

        var directive = {
            restrict: 'E',
            replace: true,
            transclude: true,
            templateUrl: 'modules/app/directives/views/app.html',
            //bindToController: true,
            //controllerAs: 'directive',
            //controller: 'directiveCtrl',
            //require: ['directive'],
            link: postLink
        };

        return directive;

        //////////////////////

        //controller.$inject = [];
        //function controller() {
        //}

        function postLink(scope, element, attrs, ctrls) {
            //var directive = ctrls[0];
        }
    }
})();