(function() {
    'use strict';

    angular
        .module('app')
        .service('Service', Service);

    Service.$inject = [];

    function Service() {

        var service = {
            func: func
        };

        return service;

        ////////////////

        function func() {
        }
    }
})();