(function() {
    'use strict';

    angular
        .module('app')
        .config(Route);

    Route.$inject = ['$stateProvider'];

    function Route($stateProvider) {

    	$stateProvider.state({
    	    name: 'home',
    	    url: '/',
    	    controller: 'Ctrl',
    	    controllerAs: 'vm',
    	    templateUrl: 'modules/app/views/home.html'
    	});

    }
})();